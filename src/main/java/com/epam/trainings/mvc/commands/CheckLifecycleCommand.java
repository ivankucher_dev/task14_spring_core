package com.epam.trainings.mvc.commands;

import com.epam.trainings.mvc.model.config.MainConfig;
import com.epam.trainings.mvc.model.otherbeans.GeneralOtherBean;
import com.epam.trainings.mvc.model.website.webpanel.Data;
import com.epam.trainings.mvc.model.website.webpanel.WebServicePanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class CheckLifecycleCommand implements Command {
  private static Logger log = LogManager.getLogger(CheckLifecycleCommand.class.getName());
  private static final String ADMIN_PROFILE = "admin";
  private static final String USER_PROFILE = "user";
  private WebServicePanel panel;
  private Data data;

  @Override
  public void execute() {
    setProfile();
    ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
    GeneralOtherBean otherBeans = context.getBean(GeneralOtherBean.class);
    testBeansScope(otherBeans);
    System.out.println("\nBy qualifier :");
    otherBeans.printByQualifier();
    System.out.println("\nBy interface :");
    otherBeans.printListBeans();
    this.panel = context.getBean(WebServicePanel.class);
    unpackPanelData(panel);
  }

  public void testBeansScope(GeneralOtherBean otherBeans) {
    log.info(otherBeans.getOtherBeanA());
    log.info(otherBeans.getOtherBeanA());
    log.info(otherBeans.getOtherBeanB());
    log.info(otherBeans.getOtherBeanB());
    log.info(otherBeans.getOtherBeanC());
    log.info(otherBeans.getOtherBeanC());
  }

  public void setProfile() {
    System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, ADMIN_PROFILE);
  }

  public void unpackPanelData(WebServicePanel panel) {
    log.info("Unpacking data ...");
    System.out.println(panel.getWebRole().getName());
    data = panel.getData();
    System.out.println(data.getAllKnownData());
  }
}
