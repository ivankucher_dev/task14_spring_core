package com.epam.trainings.mvc.model.website.webpanel;

public interface Data {
  String getAllKnownData();
}
