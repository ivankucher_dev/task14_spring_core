package com.epam.trainings.mvc.model.website.roles;

public class Admin extends Role {

  public Admin(String name, String login, int password) {
    this.login = login;
    this.password = password;
    this.name = name;
  }
}
