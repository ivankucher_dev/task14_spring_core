package com.epam.trainings.mvc.model.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("others")
@Order(1)
@Primary
@Scope("singleton")
public class OtherBeanA implements OtherBean {
  @Override
  public String getOtherBeanName() {
    return getClass().getSimpleName();
  }
}
