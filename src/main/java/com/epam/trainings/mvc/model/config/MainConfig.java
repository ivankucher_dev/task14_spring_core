package com.epam.trainings.mvc.model.config;

import com.epam.trainings.mvc.model.config.profileConfigs.AdminConfig;
import com.epam.trainings.mvc.model.config.profileConfigs.UserConfig;
import com.epam.trainings.mvc.model.website.AdminWebSiteData;
import com.epam.trainings.mvc.model.website.UserWebSiteData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan({"com.epam.trainings.mvc.model.beans1", "com.epam.trainings.mvc.model.otherbeans"})
@PropertySource({"beans.properties", "webdata.properties"})
@Import({OthersConfig.class, UserConfig.class, AdminConfig.class})
public class MainConfig {

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  public AdminWebSiteData adminWebSiteData(
      @Value("${data.users}") int online,
      @Value("${data.games}") int games,
      @Value("${data.messages}") String messages) {
    return new AdminWebSiteData(online, games, messages);
  }

  @Bean
  public UserWebSiteData userWebSiteData(
      @Value("${data.users.money}") int money,
      @Value("${data.games}") int games,
      @Value("${data.users.friends}") String friends) {
    return new UserWebSiteData(money, games, friends);
  }
}
