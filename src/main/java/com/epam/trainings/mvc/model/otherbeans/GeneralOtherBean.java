package com.epam.trainings.mvc.model.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GeneralOtherBean {
  @Autowired private List<OtherBean> beans;

  @Autowired
  @Qualifier("others")
  private List<OtherBean> qualifierBeans;

  private OtherBeanA otherBeanA;

  @Autowired
  @Qualifier("otherBeanB")
  private OtherBeanB otherBeanB;

  private OtherBeanC otherBeanC;

  @Autowired
  public GeneralOtherBean(@Qualifier("otherBeanC") OtherBeanC otherBeanC) {
    this.otherBeanC = otherBeanC;
  }

  public void printListBeans() {
    beans.forEach(otherBean -> System.out.println(otherBean));
  }

  public void printByQualifier() {
    qualifierBeans.forEach(bean -> System.out.println(bean));
  }

  @Autowired
  public void setOtherBeanA(OtherBeanA otherBeanA) {
    this.otherBeanA = otherBeanA;
  }

  public OtherBeanA getOtherBeanA() {
    return otherBeanA;
  }

  public OtherBeanB getOtherBeanB() {
    return otherBeanB;
  }

  public OtherBeanC getOtherBeanC() {
    return otherBeanC;
  }
}
