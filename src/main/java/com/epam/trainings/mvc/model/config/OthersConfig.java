package com.epam.trainings.mvc.model.config;

import com.epam.trainings.mvc.model.beans3.BeanD;
import com.epam.trainings.mvc.model.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
    basePackages = "com.epam.trainings.mvc.model.beans3",
    includeFilters =
        @ComponentScan.Filter(
            type = FilterType.ASSIGNABLE_TYPE,
            classes = {BeanD.class, BeanF.class}))
@ComponentScan(
    basePackages = "com.epam.trainings.mvc.model.beans2",
    excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = "^((?!Flower).)*"))
public class OthersConfig {}
