package com.epam.trainings.mvc.model.config.profileConfigs;

import com.epam.trainings.mvc.model.website.AdminWebSiteData;
import com.epam.trainings.mvc.model.website.roles.Admin;
import com.epam.trainings.mvc.model.website.roles.Role;
import com.epam.trainings.mvc.model.website.webpanel.Panel;
import com.epam.trainings.mvc.model.website.webpanel.WebServicePanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.epam.trainings.mvc.model.website")
@PropertySource("webdata.properties")
@Profile("admin")
public class AdminConfig {

  @Autowired AdminWebSiteData adminWebSiteData;

  @Bean
  public WebServicePanel webServicePanel(
      @Value("${admin.login}") String login, @Value("${admin.password}") int password) {
    Role admin = new Admin("admin", "admin", 1111);
    WebServicePanel panel = new Panel(admin, login, password, adminWebSiteData);
    return panel;
  }
}
