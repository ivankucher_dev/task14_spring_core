package com.epam.trainings.mvc.model.website.roles;

public abstract class Role {
  protected String name;
  protected String login;
  protected int password;

  public String getName() {
    return name;
  }

  public String getLogin() {
    return login;
  }

  public int getPassword() {
    return password;
  }
}
