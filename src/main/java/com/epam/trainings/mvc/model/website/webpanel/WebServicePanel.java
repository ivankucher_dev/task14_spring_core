package com.epam.trainings.mvc.model.website.webpanel;

import com.epam.trainings.mvc.model.website.roles.Role;

public abstract class WebServicePanel {
  protected Role webRole;
  protected String login;
  protected int password;
  protected Data data;

  public abstract boolean login();

  public abstract Data getData();

  public Role getWebRole() {
    return webRole;
  }
}
