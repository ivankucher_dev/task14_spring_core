package com.epam.trainings.mvc.model.otherbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Scope("singleton")
public class OtherBeanB implements OtherBean {
  @Override
  public String getOtherBeanName() {
    return getClass().getSimpleName();
  }
}
