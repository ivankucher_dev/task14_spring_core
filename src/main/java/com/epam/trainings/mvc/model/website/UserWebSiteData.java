package com.epam.trainings.mvc.model.website;

import com.epam.trainings.mvc.model.website.webpanel.Data;
import org.springframework.stereotype.Component;

@Component
public class UserWebSiteData implements Data {
  private int money;
  private int gamesAvaliable;
  private String friendsNum;

  public UserWebSiteData(int money, int gamesAvaliable, String friendsNum) {
    this.money = money;
    this.gamesAvaliable = gamesAvaliable;
    this.friendsNum = friendsNum;
  }

  public int getMoney() {
    return money;
  }

  public void setMoney(int money) {
    this.money = money;
  }

  public int getGamesAvaliable() {
    return gamesAvaliable;
  }

  public void setGamesAvaliable(int gamesAvaliable) {
    this.gamesAvaliable = gamesAvaliable;
  }

  public String getFriendsNum() {
    return friendsNum;
  }

  public void setFriendsNum(String friendsNum) {
    this.friendsNum = friendsNum;
  }

  @Override
  public String getAllKnownData() {
    StringBuilder sb = new StringBuilder();
    sb.append("Avaliable games : " + getGamesAvaliable());
    sb.append("\nFriends online : " + getFriendsNum());
    sb.append("\nYour cash : " + getMoney() + " uah");
    return sb.toString();
  }
}
