package com.epam.trainings.mvc.model.otherbeans;

public interface OtherBean {
  String getOtherBeanName();
}
