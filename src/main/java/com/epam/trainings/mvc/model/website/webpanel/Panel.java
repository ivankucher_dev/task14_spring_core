package com.epam.trainings.mvc.model.website.webpanel;

import com.epam.trainings.mvc.model.website.AdminWebSiteData;
import com.epam.trainings.mvc.model.website.roles.Role;
import java.nio.file.AccessDeniedException;

public class Panel extends WebServicePanel {

  private boolean dataAccess;

  public Panel(Role webRole, String login, int password, Data data) {
    this.webRole = webRole;
    this.login = login;
    this.password = password;
    this.dataAccess = login();
    this.data = data;
  }

  @Override
  public boolean login() {
    return validate();
  }

  public boolean validate() {
    if (!webRole.getLogin().equals(login) || webRole.getPassword() != password) {
      System.out.println("Login failed");
      return false;
    } else {
      System.out.println("Login successfully");
      return true;
    }
  }

  public Data getData() {
    if (dataAccess) {
      return data;
    } else {
      try {
        throw new AccessDeniedException("Failed to login,incorrect login info");
      } catch (AccessDeniedException e) {
        e.printStackTrace();
      }
      return fakeData();
    }
  }

  public AdminWebSiteData fakeData() {
    return new AdminWebSiteData(0, 0, "");
  }
}
