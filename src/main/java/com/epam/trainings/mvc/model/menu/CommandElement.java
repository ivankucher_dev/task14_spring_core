package com.epam.trainings.mvc.model.menu;

import com.epam.trainings.mvc.commands.Command;

public class CommandElement {
  private String commandName;
  private Command command;

  public CommandElement(String commandName, Command command) {
    this.commandName = commandName;
    this.command = command;
  }

  public String getCommandName() {
    return commandName;
  }

  public void setCommandName(String commandName) {
    this.commandName = commandName;
  }

  public Command getCommand() {
    return command;
  }

  public void setCommand(Command command) {
    this.command = command;
  }
}
