package com.epam.trainings.mvc.model.config.profileConfigs;

import com.epam.trainings.mvc.model.website.UserWebSiteData;
import com.epam.trainings.mvc.model.website.roles.Role;
import com.epam.trainings.mvc.model.website.roles.User;
import com.epam.trainings.mvc.model.website.webpanel.Panel;
import com.epam.trainings.mvc.model.website.webpanel.WebServicePanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.epam.trainings.mvc.model.website")
@PropertySource("webdata.properties")
@Profile("user")
public class UserConfig {

  @Autowired UserWebSiteData userWebSiteData;

  @Bean
  public WebServicePanel webServicePanel(
      @Value("${user.login}") String login, @Value("${user.password}") int password) {
    Role user = new User("user", "user", 4555);
    WebServicePanel panel = new Panel(user, login, password, userWebSiteData);
    return panel;
  }
}
