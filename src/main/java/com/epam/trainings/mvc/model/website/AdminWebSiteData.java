package com.epam.trainings.mvc.model.website;

import com.epam.trainings.mvc.model.website.webpanel.Data;
import org.springframework.stereotype.Component;

@Component
public class AdminWebSiteData implements Data {
  private int userOnlineCounter;
  private int gamesAvaliable;
  private String usersMassage;

  public AdminWebSiteData(int userOnlineCounter, int gamesAvaliable, String usersMassage) {
    this.userOnlineCounter = userOnlineCounter;
    this.gamesAvaliable = gamesAvaliable;
    this.usersMassage = usersMassage;
  }

  public int getUserOnlineCounter() {
    return userOnlineCounter;
  }

  public void setUserOnlineCounter(int userOnlineCounter) {
    this.userOnlineCounter = userOnlineCounter;
  }

  public int getGamesAvaliable() {
    return gamesAvaliable;
  }

  public void setGamesAvaliable(int gamesAvaliable) {
    this.gamesAvaliable = gamesAvaliable;
  }

  public String getUsersMassage() {
    return usersMassage;
  }

  public void setUsersMassage(String usersMassage) {
    this.usersMassage = usersMassage;
  }

  @Override
  public String getAllKnownData() {
    StringBuilder sb = new StringBuilder();
    sb.append("Avaliable games : " + getGamesAvaliable());
    sb.append("\nUsers online : " + getUserOnlineCounter());
    sb.append("\nUsers messages : " + getUsersMassage());
    return sb.toString();
  }
}
