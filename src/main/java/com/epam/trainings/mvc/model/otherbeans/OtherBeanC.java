package com.epam.trainings.mvc.model.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("others")
@Order(3)
@Scope("prototype")
public class OtherBeanC implements OtherBean {
  @Override
  public String getOtherBeanName() {
    return getClass().getSimpleName();
  }
}
