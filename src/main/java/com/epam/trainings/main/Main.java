package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.controller.ViewControllerImpl;
import com.epam.trainings.mvc.model.menu.Menu;
import com.epam.trainings.mvc.view.View;

public class Main {
  public static void main(String[] args) {
    Menu menu = new Menu();
    View view = new View(menu);
    ViewController controller = new ViewControllerImpl(view);
    view.startBySettingUpController(controller);
  }
}
